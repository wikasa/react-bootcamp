import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.unsplash.com/',
    headers: {
        Authorization: 'Client-ID S5qQ5Tf96YRQqydXiTx9690zf1E61lkr8vYU286X-nA'
    }
});