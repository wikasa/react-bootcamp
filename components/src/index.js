import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

const App = () =>{
    return(
      <div className="ui container comments">
            <ApprovalCard>
                <div>
                    <h4>Warning!</h4>
                    Are u sure?
                </div>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail 
                    author="Sam" 
                    date="Today at 4:45AM" 
                    content="Nice post!"
                    avatar={faker.image.image()}
                />
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail 
                    author="Alex" 
                    date="Today at 11:45AM" 
                    content="Woohoo!"
                    avatar={faker.image.image()}
                />
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail 
                    author="Jim" 
                    date="Today at 4:15PM" 
                    content="Good idea"
                    avatar={faker.image.image()}
                />
            </ApprovalCard>
      </div>
    );
}

ReactDOM.render(<App />, document.querySelector('#root'));