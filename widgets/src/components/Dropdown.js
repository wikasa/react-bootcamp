import React, {useState, useEffect} from 'react';

const Dropdown = ({selected, onSelectedChange, options}) => {
    const [open, setOpen] = useState(false);
    const renderedOptions = options.map((option)=>{
        if(selected.value===option.value){
            return null;
        }
        return(
            <div onClick={() => onSelectedChange(option)} key={option.value} className="item">
                {option.label}
            </div>
        );
    });
    return(
        <div className="ui form">
            <div className="field">
                <label className="label">Pick a color</label>
                <div onClick={() => setOpen(!open)} className={`ui selection dropdown ${open ? 'visible active' : ''}`}>
                    <i className="dropdown icon active"></i>
                    <div className="text">{selected.label}</div>
                    <div className={`menu ${open ? 'visible transition' : ''}`}>
                        {renderedOptions}
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Dropdown;