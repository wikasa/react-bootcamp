import axios from 'axios';
import React, {useState, useEffect} from 'react';
const createDOMPurify = require('dompurify');
const { JSDOM } = require('jsdom');

const window = new JSDOM('').window;
const DOMPurify = createDOMPurify(window);

const Search = () => {
    const [term, setTerm] = useState('algorytm');
    const [results, setResults] = useState([]);
    useEffect(() => {
        const search = async () => {
            const {data} = await axios.get('https://pl.wikipedia.org/w/api.php',{
                params:{
                    action: 'query',
                    list: 'search',
                    origin: '*',
                    format: 'json',
                    srsearch: term
                }
            });
            setResults(data.query.search);
        };
        // if (debouncedTerm) {
        //     search();
        //   }
        // }, [debouncedTerm]);
        if(term && !results.length){
            search();
        }
        else{
            const timeoutId = setTimeout(() => {
                if(term){
                    search();
                }
            }, 1000);
            return(() =>{
                clearTimeout(timeoutId);
            });
        }
    }, [term]);
    const renderedResults=results.map((result)=>{
        return(
            <div key={result.pageid} className="item">
                <div className="right floated content">
                    <a href={`http://pl.wikipedia.org/?curid=${result.pageid}`} className="ui button">Go</a>
                </div>
                <div className="content">
                    <div className="header">
                        {result.title}
                    </div>
                    <span dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(result.snippet)}}></span>
                </div>
            </div>
        );
    });
    
    return(
        <div>
            <div className="ui form">
                <div className="field">
                    <label>Search</label>
                    <input type="text" value={term} onChange={e => {setTerm(e.target.value)}} />
                    <h1>{term}</h1>
                </div>
            </div>
            <div className="ui celled list">
                    {renderedResults}
            </div>
        </div>
    );
}
export default Search;