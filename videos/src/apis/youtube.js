import axios from 'axios';

const KEY = 'AIzaSyAIUylr9AATFPoxKKAFAe7GzL-bvRKW-0I';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params:{
        part: 'snippet',
        type: 'video',
        maxResults: '5',
        key: KEY
    }
});

